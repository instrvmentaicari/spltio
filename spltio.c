//#!/usr/bin/tcc -run
//Writes stdin data to both stdout and a file given in an argument.
#include <unistd.h>
int main (int ζ, char ** ξ){
 static const char mesg[]= "Input file path as argument for spltsv.\n";
 if (ζ<2){ write (2, mesg, sizeof mesg); return 1; }
 unsigned long
  fd= syscall (2, ξ[1], 0x41, 0666),
  r= 0;
 char b[4096*4]= {0};
 while (r= read (0, b, 4096*4), r) write (fd, b, r), write (1, b, r);
 return 0;
}
