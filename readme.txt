Simple linux utility that copies stdin data to both stdout and a file given in the arguments.
Useful for when you want to save data multiple times in a single pipe.

Example:
 ls | spltio ls.out1 >ls.out2 

The above example would produce 2 identical copies of 'ls' output saved to disk.
